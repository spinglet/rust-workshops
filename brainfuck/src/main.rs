use std::fs;
use std::env::args;
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let fname = args().skip(1).next().ok_or("Expected filename")?;
    let content = fs::read_to_string(fname)?;
    let valid_characters:Vec<char> = vec!['<','>','+','-','.',',','[',']'];
    for line in content.lines() {
        for x in line.chars() {
            let x_str = x.to_string();
            if valid_characters.contains(&x) {
                print!("{}",x_str);
            }
        }
    }
    println!();
    Ok(())
}
