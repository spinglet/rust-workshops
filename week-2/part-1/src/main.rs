use std::collections::HashMap;
use std::convert::TryFrom;
use std::env::args;
use std::error;
use std::fmt;
use std::fs;

#[derive(Debug)]
enum Test {
    NameAndScore{ name: String, score: usize },
    JustName{ name: String },
}

impl std::convert::TryFrom<&str> for Test {
    // convert test description string into test enum
    type Error = Box<dyn std::error::Error>;

    fn try_from(test_description: &str) -> Result<Test, Self::Error> {
        if test_description.contains(":") {
            let v: Vec<&str> = test_description.split(":").collect();
            Ok(Test::NameAndScore{name: v[0].to_string(), score: v[1].to_string().parse()?})
        } else {
            Ok(Test::JustName{name: test_description.to_string()})
        }
    }
}

fn filename_to_tests(fname: &String) -> Result<Vec<Test>, Box<dyn std::error::Error>> {
    // take filename, return vector of people
    let content = fs::read_to_string(fname)?;
    content.lines().map(Test::try_from).collect()
}

#[derive(Default, Debug)]
struct Scores {
    total_score: usize,
    count_score: usize,
    missed_tests: usize,
}

impl Scores {
    pub fn add_score(&mut self, score: usize){
        self.total_score += score;
        self.count_score += 1;
    }

    pub fn missed_test(&mut self){
        self.missed_tests += 1;
    }
}

impl fmt::Display for Scores {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        let tests = |v| if v == 1 { "test" } else { "tests" };
        write!(
            fmt,
            "{} {}, with a total score of {}. They missed {} {}.",
            self.count_score,
            tests(self.count_score),
            self.total_score,
            self.missed_tests,
            tests(self.missed_tests)
        )
    }
}
fn main() -> Result<(), Box<dyn std::error::Error>> {
    let fname = args().nth(1).ok_or("Expected filename.")?;
    let all_tests = filename_to_tests(&fname)?;
    println!("{:#?}", all_tests);
    let mut all_scores: HashMap<String, Scores> = HashMap::new();
    // hashmap to map from name to accumulation of scores
    
    for test in all_tests {
        match test {
            Test::JustName{name} => all_scores.entry(name).or_default().missed_test(),
            Test::NameAndScore{name, score} => all_scores.entry(name).or_default().add_score(score)
        }
    }
    println!("{:#?}", all_scores);
    for (key, value) in &all_scores {
        println!("{} took {}", key, value);
    }
    Ok(())
}
