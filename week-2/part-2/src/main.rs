use std::env::args;
use std::error::Error;
use std::fmt;
use std::fs;

#[derive(Debug)]
enum RawInstruction {
    PointRight,
    PointLeft,
    Increment,
    Decrement,
    Output,
    Accept,
    JumpForwards,
    JumpBackwards,
}

impl RawInstruction {
    fn from_char(input: char) -> Option<RawInstruction> {
        match input {
            '<' => Some(RawInstruction::PointRight),
            '>' => Some(RawInstruction::PointLeft),
            '+' => Some(RawInstruction::Increment),
            '-' => Some(RawInstruction::Decrement),
            '.' => Some(RawInstruction::Output),
            ',' => Some(RawInstruction::Accept),
            '[' => Some(RawInstruction::JumpForwards),
            ']' => Some(RawInstruction::JumpBackwards),
            _ => None
        }
    }
}

#[derive(Debug)]
struct Instruction {
    input_instruction: RawInstruction,
    line_number: usize,
    character_column: usize,
}

fn filename_to_instructions(fname: &String) -> Result<Vec<Instruction>, Box<dyn std::error::Error>> {
    let content = fs::read_to_string(fname)?;
    let mut results: Vec<Instruction> = Vec::new();
    let mut line_number = 1;
    for line in content.lines() {
        let mut char_column = 1;
        for character in line.chars() {
            match RawInstruction::from_char(character) {
                Some(x) => results.push(Instruction{input_instruction: x, line_number: line_number, character_column: char_column}),
                _ => ()
            }
            char_column += 1;
        }
        line_number += 1;
    }
    Ok(results)
}

impl fmt::Display for Instruction {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(
            fmt,
            "[inputfile:{}:{}] {:?}",
            self.line_number,
            self.character_column,
            self.input_instruction,
        )
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    let fname = args().skip(1).next().ok_or("Expected filename")?;
    let content = filename_to_instructions(&fname)?;
    println!("{:#?}", content);
    for item in &content {
        println!("{}", item);
    }
    Ok(())
}
