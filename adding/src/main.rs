use std::fs;
use std::env::args;
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let fname = args().skip(1).next().ok_or("Expected filename")?;
    let content = fs::read_to_string(fname)?;
    let mut total = 0;
    for line in content.lines() {
        let line: isize = line.parse()?;
        total += line;
    }
    println!("The total is: {}", total);
    Ok(())
}
